package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/gosave/storage.git/impls"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

const scheme = "http://"

const (
	defaultHost = "127.0.0.1"
	defaultPort = "1234"
)

const (
	pathCheck = "/check"
	pathSync  = "/sync"
	pathPut   = "/put"
	pathGet   = "/get"
)

var (
	flNodeHost = flag.String("host", defaultHost, "node host")
	flNodePort = flag.String("port", defaultPort, "node port")
)

var storage impls.Storager

func init() {
	storage = impls.NewMem()
}

func check(w http.ResponseWriter, r *http.Request) {
	log.Println(pathCheck)
	resp := &impls.ResponseCheck{}
	resp.Status = "alive"
	b, _ := json.Marshal(resp)
	w.Write(b)
}

func sync(w http.ResponseWriter, r *http.Request) {
	log.Println(pathSync)
	req := &impls.RequestSync{}
	resp := &impls.ResponseSync{
		Cache: storage.RetrieveCache(),
	}

	b, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	err = json.Unmarshal(b, req)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	storage.RegisterNode(req.Address.Host, req.Address.Port)

	b, err = json.Marshal(resp)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	w.Write(b)
}

func put(w http.ResponseWriter, r *http.Request) {
	log.Println(pathPut)
	req := &impls.RequestPut{}
	resp := &impls.ResponsePut{}

	b, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	err = json.Unmarshal(b, req)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	resp.Status = storage.Put(req.Key, req.Value)
	if !req.Recursive {
		func() {
			req.Recursive = true
			b, _ = json.Marshal(req)
			for _, n := range storage.RetrieveNodes() {
				http.Post(url(scheme, n.Host, ":", n.Port, pathPut), "application/json", bytes.NewBuffer(b))
			}
		}()
	}

	b, err = json.Marshal(resp)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	fmt.Println(storage.RetrieveNodes())
	w.Write(b)
}

func get(w http.ResponseWriter, r *http.Request) {
	log.Println(pathGet)
	req := &impls.RequestGet{}
	resp := &impls.ResponseGet{}

	b, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	err = json.Unmarshal(b, req)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	resp.Value, err = storage.Get(req.Key)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	b, err = json.Marshal(resp)
	if err != nil {
		resp.ErrMsg = err.Error()
		b, _ = json.Marshal(resp)
		w.Write(b)
		return
	}

	w.Write(b)
}

func init() {
	flag.Parse()
}

func main() {
	http.HandleFunc(pathCheck, check)
	http.HandleFunc(pathSync, sync)
	http.HandleFunc(pathPut, put)
	http.HandleFunc(pathGet, get)

	listenAndServe := func(host, port string) error {
		return http.ListenAndServe(strings.Join([]string{host, port}, ":"), nil)
	}

	storage.RegisterItself(*flNodeHost, *flNodePort)
	err := listenAndServe(*flNodeHost, *flNodePort)

LOOP:
	for {
		if strings.Contains(err.Error(), "address already in use") {
			fmt.Printf("address %s:%s is already being used by another node.\ndo you want to connect to?\n(yes/no): ", *flNodeHost, *flNodePort)

			answer := ""
			fmt.Scan(&answer)

			switch answer {
			case "yes":
				host := ""
				fmt.Print("specify host: ")
				fmt.Scanln(&host)

				port := ""
				fmt.Print("specify port: ")
				fmt.Scanln(&port)

				req := &impls.RequestSync{
					Address: &impls.Address{
						Host: host,
						Port: port,
					},
				}
				resp := &impls.ResponseSync{}

				b, _ := json.Marshal(req)
				r, err := http.Post(url(scheme, *flNodeHost, ":", *flNodePort, pathSync), "application/json", bytes.NewBuffer(b))
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				json.NewDecoder(r.Body).Decode(resp)
				r.Body.Close()

				storage.RegisterItself(host, port)
				storage.RegisterNode(*flNodeHost, *flNodePort)
				storage.SetCache(resp.Cache)

				err = listenAndServe(host, port)
			default:
				break LOOP
			}
		} else {
			break LOOP
		}
	}
}

func url(ss ...string) string {
	return strings.Join(ss, "")
}
