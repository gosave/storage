package impls

type Storager interface {
	Put(key, value string) (status string)
	Get(key string) (value string, err error)
	RegisterItself(host, port string)
	RegisterNode(host, port string)
	RetrieveNodes() []Address
	RetrieveCache() map[string]string
	SetCache(cache map[string]string)
}

type node struct {
	*Address
	nodes []Address
	cache map[string]string
}

type Address struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type RequestPut struct {
	Recursive bool   `json:"recursive"`
	Key       string `json:"key"`
	Value     string `json:"value"`
}

type RequestGet struct {
	Key string `json:"key"`
}

type RequestSync struct {
	Address *Address `json:"address"`
}

type ResponsePut struct {
	ResponseError
	Status string `json:"status"`
}

type ResponseGet struct {
	ResponseError
	Value string `json:"value"`
}

type ResponseSync struct {
	ResponseError
	Cache map[string]string `json:"cache"`
}

type ResponseCheck struct {
	Status string `json:"status"`
}

type ResponseError struct {
	ErrMsg string `json:"error"`
}
