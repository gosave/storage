package impls

import (
	"errors"
)

const (
	OK  = "OK"
	ERR = "ERROR"
)

type Mem struct {
	*node
}

func (m *Mem) Put(key, value string) (status string) {
	m.cache[key] = value
	status = OK
	return
}

func (m *Mem) Get(key string) (value string, err error) {
	value, ok := m.cache[key]
	if !ok {
		err = errors.New("no value according this key")
	}
	return
}

func (m *Mem) RetrieveCache() map[string]string {
	return m.cache
}

func (m *Mem) SetCache(cache map[string]string) {
	m.cache = cache
}

func (m *Mem) RegisterItself(host, port string) {
	m.Host = host
	m.Port = port
}

func (m *Mem) RegisterNode(host, port string) {
	m.nodes = append(m.nodes, Address{
		Host: host,
		Port: port,
	})
}

func (m *Mem) RetrieveNodes() []Address {
	return m.nodes
}

func NewMem() *Mem {
	return &Mem{
		node: &node{
			Address: &Address{},
			nodes:   make([]Address, 0, 24),
			cache:   make(map[string]string),
		},
	}
}
