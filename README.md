Launch a node by compiling /cmd/api.go and executing output file.
* If there are no -host and -port flags default ones will be used. 
* If the address is busy it will be asked to specify one.
* Cache is shared between all nodes.